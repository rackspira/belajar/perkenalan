# Perkenalan

Isikan data seperti (nama,asal daerah, tempat tinggal sekarang) yang dibungkus dengan koding dengan ketentuan berikut :
- Bahasa bebas (PHP, GO, Ruby, dll)
- Tiap orang beda bahasa pemrograman

## Contoh
```
<?php
-------------- Data ------------------
$nama = 'Rackspira';
$asal = 'Saraswangi, Bantul';
$alamat = 'Janti, Bantul';

-------------- Tampilkan ------------------
$data = 'Nama : '.$nama;
$data .= '<br>';
$data .= 'Asal Daerah : '.$asal;
$data .= '<br>';
$data .= 'Tempat Tinggal : '.$alamat;

echo $data;
?>
```